<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Models\AnnouncementImage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class GoogleVisionSafeSearchImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
    * Create a new job instance.
    *
    * @return void
    */
    
    private $announcement_image_id;
    
    public function __construct($announcement_image_id)
    {
        $this->announcement_image_id=$announcement_image_id;
    }
    
    /**
    * Execute the job.
    *
    * @return void
    */
    public function handle()
    {
        $image= AnnouncementImage::find($this->announcement_image_id);
        
        if (!$image) {
            return; # code...
        }
        
        
        $image_old=file_get_contents(storage_path('/app/'. $image->file));
        
        
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('presto_google_api.json'));
        
        $imageAnnotator = new ImageAnnotatorClient();
        $response = $imageAnnotator ->safeSearchDetection($image_old);
        $imageAnnotator->close();
        
        $safe = $response->getSafeSearchAnnotation();
        
        $adult = $safe ->getAdult();
        $medical = $safe ->getMedical();
        $spoof = $safe ->getSpoof();
        $violence = $safe ->getViolence();
        $racy = $safe ->getRacy();
        
        // echo json_decode([$adult, $medical, $spoof, $violence, $racy]);
        
        $likelihoodName = ['VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE', 'LIKELY', 'VERY_LIKELY'];
        
        
        $image->adult = $likelihoodName[$adult];
        $image->medical = $likelihoodName[$medical];
        $image->spoof=$likelihoodName[$spoof];
        $image->violence=$likelihoodName[$violence];
        $image->racy=$likelihoodName[$racy];
        
        $image->save();
        
        
        
    }
}
