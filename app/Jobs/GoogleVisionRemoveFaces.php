<?php

namespace App\Jobs;

use Spatie\Image\Image;
use Illuminate\Bus\Queueable;
use Spatie\Image\Manipulations;
use App\Models\AnnouncementImage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class GoogleVisionRemoveFaces implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $announcement_image_id;
    
    public function __construct($announcement_image_id)
    {
        $this->announcement_image_id=$announcement_image_id;
    }
    
    /**
    * Execute the job.
    *
    * @return void
    */
    public function handle()
    {
        $image= AnnouncementImage::find($this->announcement_image_id);
        
        if (!$image) {
            return; # code...
        }
        
        $srcPath = storage_path('/app/'. $image->file);
        $image_old=file_get_contents($srcPath);
        
        
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('presto_google_api.json'));
        
        $imageAnnotator = new ImageAnnotatorClient();
        $response = $imageAnnotator ->faceDetection($image_old);
        $faces = $response ->getFaceAnnotations();
        
        foreach ($faces as $face) {
            $vertices = $face->getBoundingPoly()->getVertices();  
            $bounds = [];
            
            foreach ($vertices as $vertex) {
                $bounds[] = [$vertex->getx(), $vertex->getY()];   
            }   
            
            $w=$bounds[2][0] -$bounds[0][0];
            $h=$bounds[2][1] -$bounds[0][1];

            $image_old = Image::load($srcPath);
            

            $image_old->watermark(base_path('resources/img/blur.png'))
                ->watermarkPosition('top-left')
                ->watermarkPadding($bounds[0][0], $bounds[0][1] )
                ->watermarkWidth($w, Manipulations::UNIT_PIXELS)
                ->watermarkHeight($h, Manipulations::UNIT_PIXELS)
                ->watermarkFit(Manipulations::FIT_STRETCH);


                $image_old_logo = Image::load($srcPath);

                $image_old_logo->watermark(base_path('resources/img/logo.png'))
                ->watermarkOpacity(50)
                ->watermarkPosition('bottom-right')
                ->watermarkPadding(5,5, Manipulations::UNIT_PERCENT)
                ->watermarkWidth(10, Manipulations::UNIT_PERCENT)
                ->watermarkHeight(10, Manipulations::UNIT_PERCENT)
                ->watermarkFit(Manipulations::FIT_MAX);



            $image_old->save($srcPath);
            $image_old_logo->save($srcPath);
        }
        $imageAnnotator->close();
    }

}
