<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index(){

        $announcements=Announcement::where('is_accepted', true)->orderby('created_at', 'desc')->take(5)->get();
        return view('welcome', ['announcements'=>$announcements]);
    }

    public function categoriesList($name, $category_id){
        $announcements= Announcement::where([['category_id', $category_id],['is_accepted', true]])->orderby('created_at', 'desc')->paginate(6);
        return view('categories.index', compact('announcements'));
    }

    public function search(Request $request){

        $q= $request->q;
        $announcements= Announcement::search($q)->where('is_accepted', true)->get();
        return view('search_result', ['announcements'=>$announcements, 'q'=>$q]);

    }

    public function local($local){
        session()->put('local', $local);
        return redirect()->back();
    }
}