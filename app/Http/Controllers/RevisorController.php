<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;

class RevisorController extends Controller
{
   public function __construct(){
       $this->middleware('auth.revisor');
   }

    public function index(){
        $announcement = Announcement::where('is_accepted', null)->orderBy('created_at', 'desc')->first();
        return view('revisor.home', compact('announcement'));
    }

    private function setAccepted($announcement_id, $value){
        $announcement = Announcement::find($announcement_id);
        $announcement->is_accepted = $value; 
        $announcement->save();
        return redirect(route('revisor.archive'));
    }

    public function accept($announcement_id){
        return $this->setAccepted($announcement_id, true);
    }

    public function reject($announcement_id){
        return $this->setAccepted($announcement_id, false);
    }

    public function archive(){
        $announcements_accept = Announcement::where('is_accepted', true)->orderBy('updated_at', 'desc')->get();
        $announcements_reject = Announcement::where('is_accepted', false)->orderBy('updated_at', 'desc')->get();
        return view('revisor.archive', ['announcements_accept'=>$announcements_accept, 'announcements_reject'=>$announcements_reject]);
    }



}
