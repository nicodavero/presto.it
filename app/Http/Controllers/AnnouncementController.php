<?php

namespace App\Http\Controllers;


use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $uniqueSecret = $request->old('uniqueSecret', base_convert(sha1(uniqid(mt_rand())), 16, 36));

        return view('announcement.create', ['uniqueSecret' => $uniqueSecret]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementRequest $request)
    {
        $announcement = Announcement::create([
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category,
            'user_id' => Auth::id(),
            'price' => $request->price
        ]);

        $uniqueSecret = $request->uniqueSecret;
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        foreach ($images as $image) {
            $newimage = new AnnouncementImage();
            $filename = basename($image);
            $newfilename = "public/announcement/{$announcement->id}/{$filename}";

            $file = Storage::move($image, $newfilename);


            $newimage->file = $newfilename;
            $newimage->announcement_id = $announcement->id;

            $newimage->save();

            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($newimage->id),
                new GoogleVisionRemoveFaces($newimage->id),
                new ResizeImage($newimage->file, 300, 300),
                new ResizeImage($newimage->file, 640, 400),
                new ResizeImage($newimage->file, 180, 130),
                new ResizeImage($newimage->file, 200, 200)

            ])->dispatch($newimage->id);
        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
        return redirect(route('announcement.create'))->with('message', 'Il tuo annuncio è stato creato');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        return view('announcement.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    }

    public function destroyImage(Request $request)
    {
        $uniqueSecret = $request->uniquesecret;
        $filename = $request->id;
        session()->push("removedimages.{$uniqueSecret}", $filename);
        Storage::delete($filename);

        return response()->json('ok');
    }

    public function uploadImages(Request $request)
    {

        $uniqueSecret = $request->uniqueSecret;
        $filename = $request->file('file')->store("public/temp/{$uniqueSecret}");
        dispatch(new ResizeImage(
            $filename,
            120,
            120
        ));

        session()->push("images.{$uniqueSecret}", $filename);
        return response()->json(['id' => $filename]);
    }

    public function getImages(Request $request)
    {


        $uniqueSecret = $request->uniqueSecret;
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);
        $images = array_diff($images, $removedImages);

        $data = [];
        foreach ($images as $image) {
            $data[] = [
                'id' => $image,
                'src' => AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }
        return response()->json($data);
    }
}
