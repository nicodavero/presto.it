<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index'])->name('home');
Route::get('/create', [AnnouncementController::class, 'create'])->name('announcement.create');
Route::post('/store', [AnnouncementController::class, 'store'])->name('announcement.store');
Route::get('/categories/{name}/{id}', [PublicController::class, 'categoriesList'])->name('categories.index');
Route::post('/local/{local}', [PublicController::class, 'local'])->name('local');
Route::get('/show/{announcement}', [AnnouncementController::class, 'show'])->name('announcement.show');
Route::post('/announcement/images/upload', [AnnouncementController::class, 'uploadImages'])->name('announcement.images.upload');
Route::delete('/announcement/images/delete', [AnnouncementController::class, 'destroyImage'])->name('announcement.images.delete');
Route::get('/announcement/images', [AnnouncementController::class, 'getImages'])->name('announcement.images');
Route::get('/revisor/home', [RevisorController::class, 'index'])->name('revisor.index');
Route::post('/revisor/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');
Route::post('/revisor/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');
Route::get('/search', [PublicController::class, 'search'])->name('search');
Route::get('/revisor/archive', [RevisorController::class, 'archive'])->name('revisor.archive');