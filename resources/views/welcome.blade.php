<x-layout
title="Presto.it, il tuo portale di annunci">

<!-- Header -->
@if (session('access_denied'))
<div class="alert alert-danger">
  {{ session('access_denied') }}
</div>
@endif


<header>
  
  <section class="d-flex justify-content-center align-items-center section-homepage">
    
    <div class="hero">
      
      <img src="Storage{{'/media/header_presto.jpg'}}" alt="">
      
      <h1 class="headline transition">{{__('pippo.welcome')}}</h1>
      
    </div>
    
  </section>
  
</header>

{{-- Cards --}} 

{{-- TODO: CAROUSEL --}}

<section>
  
  <div class="container my-5">
    
    <div class="row">
      <div class="col-12 mb-5">
        <h1 class="text-center transition">{{__('pippo.lastAds')}}</h1>
      </div>
    </div>
    
    {{-- <div class="row justify-content-center">
      @foreach ($announcements as $announcement)
      <div class="col-lg-7 col-md-10 col-11 card-style p-2 transition my-3">
        
        <div class="row">
          
          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center">
            <div id="carouselExampleIndicators{{$announcement->id}}" class="carousel slide" data-bs-ride="carousel">
              
              <div class="carousel-inner">
                @foreach ($announcement->images as $image)
                <div class="carousel-item @if($loop->index == 0)
                  active
                  @endif
                  ">
                  <img src="{{$image->getUrl(180, 130)}}" class="d-block" alt="...">
                </div>
                @endforeach
                
                
              </div>
              <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators{{$announcement->id}}" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators{{$announcement->id}}" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
          </div>
          
          <div class="col-lg-9 col-md-8 col-6 px-1">
            
            <div class="col-12">
              
              <h4 class="fw-bold mb-0 card-title">{{$announcement->title}}</h4>
              
            </div>
            
            <div class="col-12">
              
              <a href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}" class="text-decoration-none"><span class="text-uppercase tag-card tag-teal-card">{{$announcement->category->name}}</span></a> 
              
            </div>
            
            <div class="col-12">
              
              <h6 class="fw-bold mb-0 pt-1 px-1 card-price" style="color: grey;">€ {{$announcement->price}}</h6>
              
            </div>
            
            <div class="col-12 d-flex justify-content-end mt-md-4 pt-md-2 mt-2 px-md-1">
              
              <a href="{{route('announcement.show', compact('announcement'))}}"><button type="button" class="fw-bold text-uppercase card-button">{{__('pippo.details')}}</button></a>
              
            </div>
            
          </div>
          
        </div>
        
      </div>     
      @endforeach
    </div> --}}


      <div class="splide">
        <div class="splide__track">
          <div class="splide__list">
            
            @foreach ($announcements as $announcement)
            <div class="splide__slide">
              
              
              <div class="row">
                <div class="col-md-6 col-12">
                  <div id="carouselExampleIndicators{{$announcement->id}}" class="carousel slide" data-bs-ride="carousel">               
                    <div class="carousel-inner">
                      @foreach ($announcement->images as $image)
                      <div class="carousel-item @if($loop->index == 0)
                        active
                        @endif
                        ">
                        <img src="{{$image->getUrl(640, 400)}}" class="d-block" alt="...">
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
                
                <div class="col-md-5 col-12 px-1">
                  
                  <div class="row ms-0 ms-md-5 mt-5" >
                    <div class="col-12">
                    
                    <h2 class="fw-bold mb-0 card-title">{{$announcement->title}}</h2>
                    
                  </div>
                    <div class="col-12 mt-3">
                    
                    <h4 class="fw-bold mb-0 pt-1 px-1 card-price" style="color: grey;">€ {{$announcement->price}}</h4>
                    
                  </div> 
                  <div class="col-12 mt-3 " >
                    
                    <a href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}" class="text-decoration-none"><span class="text-uppercase tag-card tag-teal-card " style="font-size:14px;">{{$announcement->category->name}}</span></a> 
                    
                  </div>

                   <div class="col-12 mt-md-4 pt-md-2 mt-4 px-md-1" style="height:50px;">
                    
                    <a style="border-radius: 39px 39px 39px 39px;
                    padding: 13px;
                    border: 2px solid #ffc400;" href="{{route('announcement.show', compact('announcement'))}}"><button type="button" class="fw-bold text-uppercase card-button">{{__('pippo.details')}}</button></a>
                    
                  </div>
                  </div>
                  
               
                  
                  
                  
                  
                  
                </div>
                
                
              </div>
              
              
              
            </div>
            
            @endforeach
          </div>
          
          
        </div>
        
        
        
      </div>
    </section>
    
    <section class="my-6">
      
      <div class="container ">
        
        <div class="row">
          
          <div class="col-12 d-flex justify-content-center">
            
            <h2>{{__('pippo.sell')}} <p class="d-inline yellow-presto">Presto</p>{{__('pippo.steps')}}:</h2>
            
          </div>
          
        </div>
        
        <div class="row justify-content-center mt-5">
          
          <div class="col-md-3 col-8">
            
            <div class="row">
              <div class="col-md-6 col-12 d-flex justify-content-center align-items-center">
                <i class="far fa-handshake fs-1 yellow-icon"></i>
              </div>
              <div class="col-md-6 col-12 d-flex align-items-center justify-content-center justify-content-md-start">
                <p class="fw-bold mt-3 mt-md-0">{{__('pippo.deal')}}</p>
              </div>
            </div>
            <div class="row mt-5">
              <div class="col-md-6 col-12 d-flex justify-content-center align-items-center">
                <i class="fas fa-box-open fs-1 yellow-icon"></i>
              </div>
              <div class="col-md-6 col-12 d-flex align-items-center justify-content-center justify-content-md-start">
                <p class="fw-bold mt-3 mt-md-0">{{__('pippo.box')}}</p>
              </div>
            </div>
            <div class="row mt-5">
              <div class="col-md-6 col-12 d-flex justify-content-center align-items-center">
                <i class="fas fa-shipping-fast fs-1 yellow-icon"></i>
              </div>
              <div class="col-md-6 col-12 d-flex align-items-center justify-content-center justify-content-md-start">
                <p class="fw-bold mt-3 mt-md-0">{{__('pippo.delivery')}}</p>
              </div>
            </div>
            
          </div>
          
        </div>
        
      </div>
      
    </section> 
    
    <script>
      
      
      document.addEventListener( 'DOMContentLoaded', function () {
        new Splide( '.splide', {
          type   : 'loop',
          padding: {
            right: '5rem',
            left : '5rem',
          }, 
          breakpoints: {
		1920: {
     
		}
          },
          breakpoints: {
		640: {
			perPage: 1,
      width:500,
      padding: {
            right: 0,
            left : 0,
          },
		}
          },

      

         
        }).mount();
      })
      
      
    </script>
    
  </x-layout>