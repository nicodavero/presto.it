<x-layout>

    <div class="container-fluid my-5 justify-content-center transition min-height">
        <div class="row justify-content-center mt-4">
            <div class="col-lg-5 col-11 p-0">
              <div id="carouselExampleIndicators{{$announcement->id}}" class="carousel slide" data-bs-ride="carousel">
                
                <div class="carousel-inner">
                  @foreach ($announcement->images as $image)
                  <div class="carousel-item @if($loop->index == 0)
                    active
                    @endif
                    ">
                    <img src="{{$image->getUrl(640, 400)}}" class="d-block img-fluid" alt="...">
                  </div>
                  @endforeach
                  
                  
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators{{$announcement->id}}" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators{{$announcement->id}}" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
            </div>
            <div class="col-lg-3 col-11 d-flex flex-column mt-4 mt-lg-0 p-0 px-lg-4">
                <div class="col-12 px-lg-3">
                    <a href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}" class="text-decoration-none"><span class="tag-show tag-teal-show text-uppercase">{{$announcement->category->name}}</span></a> 
                </div>
                <div class="col-12 mt-3 px-lg-3">
                    <h4 class="fw-bold">{{$announcement->title}}</h4>
                </div>
                <div class="col-12 mt-2 px-lg-3">
                  <h4 class="price fw-bold" style="color: grey">€ {{$announcement->price}}</h4>
                </div>
                <div class="col-12 mt-lg-auto px-lg-3 mt-3">
                  <h6 class="mb-md-0 fw-bold">Aggiunto il: {{$announcement->created_at->format('d/m/Y')}}</h6>
                </div>
                <div class="col-12 px-lg-3 mt-lg-2">
                    <h6 class="mb-lg-0 fw-bold">Inserzionista: {{$announcement->user->name}}</h6>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-lg-4 mt-3">
            <div class="col-lg-8 col-11 top-line p-0">
                <h4 class="mt-lg-2 mt-3 fw-bold">Descrizione</h4>
                <p>{{$announcement->description}}</p>
            </div>
        </div>
    </div>
   
</x-layout>