<x-layout>

    <section>

      <div class="container transition">
        <div class="row">
          <h1 class="col-12 col-md-6 my-5 offset-md-3 px-2 text-center transition"><p class="d-inline yellow-presto">Presto</p>, inserisci il tuo annuncio!</h1>
        </div>
        
        
        @if (session('message'))
        <div class="alert alert-success">
          {{ session('message') }}
        </div>
        @endif

        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif


        <div class="row">
          <form action="{{route('announcement.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="col-12 col-md-6 mb-4 offset-md-3 px-2 transition">
          <label for="exampleFormControlInput1" class="form-label">{{__('pippo.create_title')}}</label>
          <input type="text" class="form-control" id="exampleFormControlInput1" name="title" value="{{old('title')}}">
        </div>
        
          <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">

           <div class="col-12 col-md-6 mb-3 offset-md-3 px-2 transition">
          <label for="exampleFormControlTextarea1" class="form-label">{{__('pippo.create_description')}}</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="3">{{old('description')}}</textarea>
          </div>
            
          <div class="col-12 col-md-6 mb-4 offset-md-3 px-2 transition">
            <label for="exampleInputEmail1" class="form-label">{{__('pippo.create_price')}}</label>
            <input type="decimal" class="form-control" value="{{old('price')}}" name="price">
          </div> 

            <div class=" col-12 col-md-6 offset-md-3 px-2 transition">
            <select class="form-select" name="category" aria-label="Default select example">
              <option value="" selected>{{__('pippo.create_category')}}</option>
              @foreach ($categories as $category)
              <option 
              @if (old('category') == $category->id)
                  selected
              @endif
              value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
            @error('category')
                <span class="invalid-feedback" role="alert">
                  <strong>{{$message}}</strong>
                </span>
            @enderror
          </div>

          <div class="col-12 col-md-6 mb-4 offset-md-3 my-4 px-2 transition">
            <label for="exampleInputEmail1" class="form-label">{{__('pippo.create_images')}}</label>
           <div id="drophere" class="dropzone"></div>
          </div> 



          <div class="col-12 d-flex justify-content-center my-5 pt-md-2 px-md-1">
            <button type="submit" class="fw-bold text-uppercase">{{__('pippo.create_submit')}}</button>
          </div>
          </form>
        </div>
      </div>

  </section>

  </x-layout>