<x-layout>

    <div class="container-fluid my-5 transition min-height">

        <div class="row">
          <div class="col-12 my-3">
            <h2 class="text-center fw-bold">Ecco i risultati per: {{$q}}</h2>
          </div>
        </div>
  
        <div class="row my-3 justify-content-center">       
          @forelse ($announcements as $announcement)
          <div class="col-lg-7 col-md-10 col-11 card-style p-2">
            <div class="row">

                <div class="col-lg-3 col-md-4 col-6">
                  
                  <img class="img-fluid img-card" src="./media/pexels-ivan-samkov-7620714.jpg" alt="{{$announcement->title}}">
                
                </div>
        
                <div class="col-lg-9 col-md-8 col-6 px-1">
        
                    <div class="col-12">
                  
                      <h4 class="fw-bold mb-0 card-title">{{$announcement->title}}</h4>
                  
                    </div>
                  
                    <div class="col-12">
                  
                      <a href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}" class="text-decoration-none"><span class="text-uppercase tag-card tag-teal-card">{{$announcement->category->name}}</span></a> 
                  
                    </div>
                  
                    <div class="col-12">
                  
                      <h6 class="fw-bold mb-0 pt-1 px-1 card-price" style="color: grey;">€ {{$announcement->price}}</h6>
                  
                    </div>
                  
                    <div class="col-12 d-flex justify-content-end mt-md-4 pt-md-2 mt-2 px-md-1">
                  
                      <a href="{{route('announcement.show', compact('announcement'))}}"><button type="button" class="fw-bold text-uppercase card-button">Scopri di più</button></a>
                  
                    </div>
        
                </div>
    
            </div>

            @empty
            <h2 class="text-center">Non c'è nessun annuncio</h2>
            <i class="far fa-frown sad-smile my-3 d-flex justify-content-center card-transition"></i>
          @endforelse
        </div> 
    </div>













{{-- <h1 class="text-center mt-5 card-transition">Ecco i risultati per: {{$q}}</h1>

<div class="container card-transition">
    <div class="row">
        @forelse ($announcements as $key=>$announcement)
        <div class="col-md-4 col-12 my-3">
            <div class="card" style="width: 18rem;">
                <img src="https://picsum.photos/300" class="card-img-top" alt="{{$announcement->title}}">
                <div class="card-body">
                    <h5 class="card-title">{{$announcement->title}}</h5>
                    <p class="card-text">{{$announcement->description}}</p>
                    <div class="row my-3">
                        <div class="col-6">
                            {{$announcement->created_at->format('d/m/Y')}}
                        </div>
                        <div class="col-6 text-end">
                            <a href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}">{{$announcement->category->name}}</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-2">
                            <strong>Prezzo:</strong> €{{$announcement->price}}
                          </div>
                        <div class="col-12 my-2">
                            Creato da: {{$announcement->user->name}}
                        </div>
                    </div>
                    <a href="#" class="btn btn-primary">Scopri di più</a>
                </div>
            </div>
        </div>
        @empty
        <h2 class="text-center mt-5">Non c'è nessun annuncio</h2>
        <i class="far fa-frown sad-smile my-5 d-flex justify-content-center card-transition"></i>
        @endforelse
    </div> 
</div> --}}

</x-layout>