<x-layout

title="Presto.it, il tuo portale di annunci">

    <main>
    
        <div class="container transition min-height">
            <div class="row">
              <h1 class="col-12 col-md-6 my-5 offset-md-3 px-2 text-center">Login</h1>
            </div>
            
            <div class="row">
              <form action="{{route('login')}}" method="POST" >
                @csrf
               
                <div class="col-12 col-md-6 mb-4 offset-md-3 px-2 text-boxes">
                  <label for="exampleInputEmail1" class="form-label">Email</label>
                  <input type="email" class="form-control" value="" name="email">
                </div>
                <div class="col-12 col-md-6 mb-4 offset-md-3 px-2 text-boxes">
                  <label for="exampleInputEmail1" class="form-label">Password</label>
                  <input type="password" class="form-control" value=""  name="password">                
                </div>
               
                <div class="col-12 d-flex justify-content-center my-5 pt-md-2 px-md-1">
                  <button type="submit" class="fw-bold text-uppercase">Invia</button>
                </div>
              </form>
            </div>
        </div>
    
    </main>
    
    </x-layout>