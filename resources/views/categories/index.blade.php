<x-layout
title="Presto.it, il tuo portale di annunci">

<div class="container-fluid my-5 transition min-height">

   <div class="row justify-content-center">
    @forelse ($announcements as $announcement)   
        <div class="col-lg-7 col-md-10 col-11 card-style p-2 my-3">

            <div class="row">

                <div class="col-lg-4 col-md-4 col-6">
                    <div id="carouselExampleIndicators{{$announcement->id}}" class="carousel slide" data-bs-ride="carousel">
                
                        <div class="carousel-inner">
                          @foreach ($announcement->images as $image)
                          <div class="carousel-item @if($loop->index == 0)
                            active
                            @endif
                            ">
                            <img src="{{$image->getUrl(180, 130)}}" class="d-block" alt="...">
                          </div>
                          @endforeach
                          
                          
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators{{$announcement->id}}" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators{{$announcement->id}}" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </button>
                      </div>
               
                
                </div>
        
                <div class="col-lg-8 col-md-8 col-6 px-1">
        
                    <div class="col-12">
                
                    <h4 class="fw-bold mb-0 card-title">{{$announcement->title}}</h4>
                
                    </div>
                
                    <div class="col-12">
                
                    <a href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}" class="text-decoration-none"><span class="tag-card tag-teal-card text-uppercase">{{$announcement->category->name}}</span></a> 
                
                    </div>
                
                    <div class="col-12">
                
                    <h6 class="fw-bold mb-0 pt-1 px-1 card-price" style="color: grey;">€ {{$announcement->price}}</h6>
                
                    </div>
                
                    <div class="col-12 d-flex justify-content-end mt-md-4 pt-md-2 mt-2 px-md-1">
                
                    <a href="{{route('announcement.show', compact('announcement'))}}"><button type="button" class="fw-bold text-uppercase card-button">Scopri di più</button></a>
                
                    </div>
        
                </div>

            </div>

        </div>
      
        @empty        
        <h2 class="text-center card-transition mt-5">Non ci sono annunci disponibili per questa categoria</h2>
        <i class="far fa-frown sad-smile my-3 d-flex justify-content-center card-transition"></i>
        @endforelse
        <div class="row">
            <div class="col-md-4 arrow">
                {{$announcements->render();}}
            </div>
        </div>
    </div>
</div>




























{{-- <div class="container my-5">
    <div class="row">
        @forelse ($announcements as $announcement)
        <div class="col-md-4 col-12 my-3">
            <div class="card card-transition" style="width: 18rem">
                <img src="https://picsum.photos/300" class="card-img-top" alt="{{$announcement->title}}">
                <div class="card-body">
                    <h5 class="card-title fw-bold text-center">{{$announcement->title}}</h5>
                    <p class="card-text text-center">{{$announcement->description}}</p>
                    <div class="row my-3">
                        <div class="col-6 px-2">
                            {{$announcement->created_at->format('d/m/Y')}}
                        </div>
                        <div class="col-6 text-end px-2">
                            <a class="card-link" href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}">{{$announcement->category->name}}</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-2">
                            <b>Prezzo:</b> €{{$announcement->price}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-2">
                            Creato da: {{$announcement->user->name}}
                        </div>
                    </div>
                    
                    <div class="frame col-12 col-md-6 offset-md-3 px-2 text-boxes mt-3 mb-0">
                        <a href="{{route('announcement.show', compact('announcement'))}}"><button type="submit" class="custom-btn btn-15 text-uppercase">Scopri di più</button></a>
                    </div>
                </div>
            </div>
        </div>
        @empty        
        <h2 class="text-center card-transition mt-5">Non ci sono annunci disponibili per questa categoria</h2>
        <i class="far fa-frown sad-smile my-5 d-flex justify-content-center card-transition"></i>
        @endforelse
        <div class="row">
            <div class="col-md-4 arrow">
                {{$announcements->render();}}
            </div>
        </div>
    </div>
</div>
 --}}

</x-layout>