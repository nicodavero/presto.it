<x-layout>
 
  @if ($announcement)

  <div class="container my-5 transition min-height">
      <div class="row justify-content-center">
          <div class="col-lg-9 col-11 d-flex justify-content-center p-0" style="background-color: rgb(255, 196, 0);">
            <h6 class="text-uppercase fw-bold mb-0 p-1" style="color: rgb(250, 250, 250);">Annuncio {{$announcement->id}}</h6>
          </div>
      </div>
      <div class="row justify-content-around mt-5">
          <div class="col-lg-3 col-11 border rounded text-center p-1">
              <h6 class="text-uppercase fw-bold yellow py-2">Titolo</h6>
              <p class="h4 fw-bold">{{$announcement->title}}</p>
          </div>
          <div class="col-lg-3 col-11 border rounded text-center p-1 mt-5 mt-lg-0">
              <h6 class="text-uppercase fw-bold yellow py-2">Prezzo</h6>
              <p class="h4 fw-bold">€ {{$announcement->price}}</p>
          </div>  
      </div>
      <div class="row justify-content-around mt-5">
          <div class="col-lg-3 col-11 border rounded text-center p-1">
              <h6 class="text-uppercase fw-bold yellow py-2">Aggiunto il</h6>
              <p class="h4 fw-bold">{{$announcement->created_at->format('d/m/Y')}}</p>
          </div>
          <div class="col-lg-3 col-11 border rounded text-center p-1 mt-5 mt-lg-0">
              <h6 class="text-uppercase fw-bold yellow py-2">Inserzionista</h6>
              <p class="h4 fw-bold">{{$announcement->user->name}}</p>
          </div>
      </div>
      <div class="row justify-content-around mt-5">
          <div class="col-lg-3 col-11 border rounded text-center p-1">
              <h6 class="text-uppercase fw-bold yellow py-2">User ID</h6>
              <p class="h4 fw-bold">{{$announcement->user->id}}</p>
          </div>
          <div class="col-lg-3 col-11 border rounded text-center p-1 mt-5 mt-lg-0">
              <h6 class="text-uppercase fw-bold yellow py-2">User E-mail</h6>
              <p class="h4 fw-bold">{{$announcement->user->email}}</p>
          </div>
      </div>
      <div class="row justify-content-center mt-5">
          <div class="col-lg-9 col-11 border rounded text-center p-1">
              <h6 class="text-uppercase fw-bold yellow py-2">Descrizione</h6>
              <p class="h6">{{$announcement->description}}</p>
          </div>
      </div>
      <div class="row justify-content-center">
          @foreach ($announcement->images as $image)
          <div class="col-md-9 col-11 border rounded text-center p-1 mt-5">
              <div class="row">
                  <div class="col-md-4 col-12">
                      <h6 class="text-uppercase fw-bold yellow py-2">Immagine</h6>
                      <div class="mt-4">
                        <img class="img-fluid" src="{{$image->getUrl(200, 200)}}">
                      </div>
                  </div>
                  <div class="col-md-4 col-5 mt-4 mt-md-0">
                      <h6 class="text-uppercase fw-bold yellow py-2">Argomenti</h6>
                      <div class="mt-4">
                          <ul class="label-api px-0">
                              @if ($image->labels)
                              @foreach ($image->labels as $label)
                              <li>{{$label}}</li>
                              @endforeach
                              @endif
                          </ul>
                      </div>
                  </div>
                  <div class="col-md-4 col-7 mt-4 mt-md-0">
                      <h6 class="text-uppercase fw-bold yellow py-2">Controllo</h6>
                      <div class="mt-4">
                          <p>Contenuti per adulti: <i class="fas fa-circle {{$image->adult}}"></i> </p>
                          <p>Contenuto satirico: <i class="fas fa-circle {{$image->spoof}}"></i></p>
                          <p>Contenuto medico: <i class="fas fa-circle {{$image->medical}}"></i></p>
                          <p>Contenuto violento: <i class="fas fa-circle {{$image->violence}}"></i></p>
                          <p>Contenuto spinto: <i class="fas fa-circle {{$image->racy}}"></i></p>
                      </div>
                  </div>
              </div>
          </div>
          @endforeach
      </div>

      <div class="row justify-content-center mt-5">
          <div class="col-lg-2 col-md-5 col-11 d-flex justify-content-around">
            <form action="{{route('revisor.accept', $announcement->id)}}" method="POST">
              @csrf
              <button type="submit" class="btn btn-success">Accetta</button></form>
              <form action="{{route('revisor.reject', $announcement->id)}}" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger">Rifiuta</button></form>
              </div>
            </div>
          </div>
          @else
          <h2 class="text-center mt-5 transition">Non ci sono annunci da revisionare</h2>    
      @endif

      <div class="row my-5 transition mx-0">
          <div class="col-12 d-flex justify-content-center">
            <a class="" href="{{route('revisor.archive')}}"><button type="submit" class="button-archive text-uppercase">Il tuo archivio</button></a>
          </div>
      
  </div>
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  {{-- @if ($announcement)
  <div class="container-fluid my-5 justify-content-center transition">
    <div class="row justify-content-center">
      <div class="col-lg-8 col-11 d-flex justify-content-center p-0" style="background-color: rgba(255, 196, 0, 0.9);">
        <h6 class="text-uppercase fw-bold mb-0 p-1" style="color: rgb(250, 250, 250);">Annuncio {{$announcement->id}}</h6>
      </div>
    </div>
    <div class="row justify-content-center mt-4">
      <div class="col-md-5 col-11 p-0">
        <div id="carouselExampleIndicators{{$announcement->id}}" class="carousel slide" data-bs-ride="carousel">
          
          <div class="carousel-inner">
            @foreach ($announcement->images as $image)
            <div class="carousel-item @if($loop->index == 0)
              active
              @endif
              ">
              <img src="{{$image->getUrl(600, 400)}}" class="d-block" alt="...">
            </div>
        </div>
        <div class="row justify-content-center mt-4">
            <div class="col-lg-5 col-11">
              <div class="row">
                <div class="col-12 revisor-border">
                  <div class="row">
                    <div class="col-4 d-flex justify-content-center">Immagine</div>
                    <div class="col-4 d-flex justify-content-center">Tags</div>
                    <div class="col-4 d-flex justify-content-center">Api</div>
                  </div>                  
                </div>
              </div>
            
            </div>
          <div class="col-lg-3 col-11 d-flex flex-column mt-4 mt-lg-0 p-0 px-lg-4">
            <div class="col-12 px-lg-3">
                <a href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}" class="text-decoration-none"><span class="tag-show tag-teal-show text-uppercase">{{$announcement->category->name}}</span></a> 
            </div>
            <div class="col-12 mt-3 px-lg-3">
                <h4 class="fw-bold">{{$announcement->title}}</h4>
            </div>
            <div class="col-12 mt-2 px-lg-3">
              <h4 class="price fw-bold" style="color: grey">€ {{$announcement->price}}</h4>
            </div>
            <div class="col-12 mt-lg-auto px-lg-3">
              <h6 class="fw-bold">Aggiunto il: {{$announcement->created_at->format('d/m/Y')}}</h6>
            </div>
            <div class="col-12 px-lg-3">
                <h6 class="fw-bold">Inserzionista: {{$announcement->user->name}}</h6>
            </div>
            <div class="col-12 px-lg-3">
              <h6 class="fw-bold">User ID: {{$announcement->user->id}}</h6>
            </div>
            <div class="col-12 px-lg-3">
              <h6 class="mb-lg-0 fw-bold">User E-mail: {{$announcement->user->email}}</h6>
            </div>
          </div>
        <div class="row justify-content-center mt-lg-4 mt-3">
            <div class="col-lg-8 col-11 top-line p-0">
                <h4 class="mt-lg-2 mt-3 fw-bold">Descrizione</h4>
                <p>{{$announcement->description}}</p>
            </div>
          
            <p>Adult: {{$image->adult}}</p>
            <p>Spoof: {{$image->spoof}}</p>
            <p>Medical: {{$image->medical}}</p>
            <p>Violence: {{$image->violence}}</p>
            <p>Racy: {{$image->racy}}</p>
            <ul>
              
                @if ($image->labels)
            @foreach ($image->labels as $label)
            <li>{{$label}}</li>
            @endforeach
            @endif
            </ul>
          
            
            @endforeach
            
            
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators{{$announcement->id}}" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators{{$announcement->id}}" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
        
      </div>
      <div class="col-md-3 col-11 text-right d-flex flex-column mt-4 mt-lg-0 p-0">
        <div class="col-12 px-lg-3">
          <a href="{{route('categories.index', ['name'=>$announcement->category->name, 'id'=>$announcement->category->id])}}" class="text-decoration-none"><span class="tag-show tag-teal-show text-uppercase">{{$announcement->category->name}}</span></a> 
        </div>
        <div class="col-12 mt-3 px-lg-3">
          <h4 class="fw-bold">{{$announcement->title}}</h4>
        </div>
        <div class="col-12 mt-2 px-lg-3">
          <h4 class="price fw-bold" style="color: grey">€ {{$announcement->price}}</h4>
        </div>
        <div class="col-12 mt-lg-auto px-lg-3">
          <h6 class="fw-bold">Aggiunto il: {{$announcement->created_at->format('d/m/Y')}}</h6>
        </div>
        <div class="col-12 px-lg-3">
          <h6 class="fw-bold">Inserzionista: {{$announcement->user->name}}</h6>
        </div>
        <div class="col-12 px-lg-3">
          <h6 class="fw-bold">User ID: {{$announcement->user->id}}</h6>
        </div>
        <div class="col-12 px-lg-3">
          <h6 class="mb-lg-0 fw-bold">User E-mail: {{$announcement->user->email}}</h6>
        </div>
      </div>
      <div class="row justify-content-center mt-lg-4 mt-3">
        <div class="col-lg-8 col-11 top-line p-0">
          <h4 class="mt-lg-2 mt-3 fw-bold">Descrizione</h4>
          <p>{{$announcement->description}}</p>
        </div>
      </div>
      <div class="row justify-content-center mt-4">
        <div class="col-lg-2 col-md-5 col-11 d-flex justify-content-around">
          <form action="{{route('revisor.accept', $announcement->id)}}" method="POST">
            @csrf
            <button type="submit" class="btn btn-success">Accetta</button></form>
            <form action="{{route('revisor.reject', $announcement->id)}}" method="POST">
              @csrf
              <button type="submit" class="btn btn-danger">Rifiuta</button></form>
            </div>
          </div>
        </div>
        @else
        <h2 class="text-center mt-5 transition">Non ci sono annunci da revisionare</h2>    
    @endif

    <div class="row my-5 transition">
        <div class="col-12 d-flex justify-content-center">
          <a class="" href="{{route('revisor.archive')}}"><button type="submit" class="button-archive text-uppercase">Il tuo archivio</button></a>
        </div> --}}
        
      </x-layout>