<x-layout>

  <div class="container my-5 transition">
    <div class="row">
      <div class="col-12">
        <h2 class="text-center">Annunci rifiutati</h2>
      </div>
    </div>

    <div class="row">
     @forelse($announcements_reject as $announcement_reject)
        <div class="col-md-3 col-12 mt-4">
          <div class="card card-transition">
            <div class="card-header text-center">Annuncio {{$announcement_reject->id}}</div>
            <div id="carouselExampleIndicators{{$announcement_reject->id}}" class="carousel slide" data-bs-ride="carousel">
              <div class="carousel-inner">
                @foreach ($announcement_reject->images as $image)
                  <div class="carousel-item @if($loop->index == 0)
                        active
                        @endif
                        ">
                        <img src="{{$image->getUrl(300, 300)}}" class="d-block w-100" alt="...">
                  </div>
                @endforeach
                      
              </div>
              <button class="carousel-control-prev" type="button"  data-bs-target="#carouselExampleIndicators{{$announcement_reject->id}}" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators{{$announcement_reject->id}}" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-6 text-center">
                       <p>User ID: {{$announcement_reject->user->id}}</p>
                </div>
                <div class="col-6 text-center">
                        <p>User: {{$announcement_reject->user->name}}</p> 
                </div>
              </div>
              <div class="col-12 d-flex justify-content-center mt-1">
                       <p>E-mail: {{$announcement_reject->user->email}}</p> 
              </div>
              <h5 class="card-title text-center mt-1">{{$announcement_reject->title}}</h5>                
              <div class="row">
                <div class="col-12 text-center mt-1">
                  <b>Prezzo:</b> €{{$announcement_reject->price}}
                </div>
              </div>
              <div class="row d-flex justify-content-center">
                <div class="col-6 d-flex justify-content-center mt-4">
                  <form action="{{route('revisor.accept', $announcement_reject->id)}}" method="POST">
                  @csrf
                  <button type="submit" class="btn btn-success">Accetta</button></form>
                </div>
                        
              </div>
            </div>
          </div>
       </div>
                
       @empty
        <h2 class="text-center mt-5 card-transition">Non ci sono annunci nel tuo archivio</h2>   
        @endforelse
     </div>
  </div>
  
</x-layout>