<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted transition">
    <!-- Section: Social media -->
    <section class="p-4 border-bottom">
      <!-- Center -->
      <div class="row justify-content-center">
        <div class="col-5 d-flex align-items-center">
          <a href="https://it-it.facebook.com/" class="me-4 text-reset">
            <i class="fab fa-facebook-f"></i>
          </a>
          <a href="https://twitter.com/?lang=it" class="me-4 text-reset">
            <i class="fab fa-twitter"></i>
          </a>
          <a href="https://www.instagram.com/" class="me-4 text-reset">
            <i class="fab fa-instagram"></i>
          </a>
          </a>
          <a href="https://github.com/" class="me-4 text-reset">
            <i class="fab fa-github"></i>
          </a>
        </div>
        <div class="col-5 d-flex justify-content-end align-items-center">
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle language" href="#" id="navbarDropdown" role="button"
                  data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-globe"></i></a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                 
                <li class="d-inline-block">
                  <form action="{{route('local', 'it')}}" method="POST">
                      @csrf
                      <button type="submit" class="languages-box">
                          <span class="flag-icon flag-icon-it"></span>
                      </button>
                  </form>
                </li>
                      
                <li class="d-inline-block">
                  <form action="{{route('local', 'gb')}}" method="POST">
                      @csrf
                      <button type="submit" class="languages-box">
                          <span class="flag-icon flag-icon-gb"></span>
                      </button>
                  </form>
                </li>
                          
                <li class="d-inline-block">
                  <form action="{{route('local', 'fr')}}" method="POST">
                      @csrf
                    <button type="submit" class="languages-box">
                        <span class="flag-icon flag-icon-fr"></span>
                    </button>
                  </form>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      <!-- Center -->
    </section>
  
    <!-- Section: Links  -->
    <section class="">
      <div class="container text-center text-md-start mt-5">
        <!-- Grid row -->
        <div class="row mt-3 justify-content-around">
          <!-- Grid column -->
          <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
            <!-- Content -->
            <h6 class="text-uppercase fw-bold mb-4">
                <i class="fab fa-phoenix-squadron logo-footer fw-bold"></i><span class="logo-footer mx-2">Presto.it</span>
            </h6>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem delectus, labore, magni ipsa esse iste deleniti corporis perspiciatis excepturi eaque, laboriosam eligendi cumque provident optio aliquid? Maiores dicta placeat fugiat?
            </p>
          </div>
  
          <!-- Grid column -->
          <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              {{__('pippo.categories')}}
            </h6>

            @for ($i = 0; $i < 4; $i++)
            <p>                  
              <a href="#!" class="text-reset">{{$categories[$i]->name}}</a>
            </p>
            @endfor
            
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              Account
            </h6>
            @guest
            <p>
              <a href="{{ route('register') }}" class="text-reset">Registrati</a>
            </p>
            <p>
              <a href="{{ route('login') }}" class="text-reset">Login</a>
            </p>
            @else
            <p>
                <a class="text-reset" href="{{ route('logout') }}"
                onclick="event.preventDefault();document.getElementById('form-logout').submit();">Logout</a>
            </p>
            <form action="{{ route('logout') }}" method="POST" id="form-logout">
                @csrf
            </form>
            @endguest
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              {{__('pippo.contacts')}}
            </h6>
            <p><i class="fas fa-home me-3"></i> Capracotta, IS 86082</p>
            <p>
              <i class="fas fa-envelope me-3"></i>
              info@presto.it
            </p>
            <p><i class="fas fa-phone me-3"></i> 865 777 900</p>
            
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
      </div>
    </section>
    <!-- Section: Links  -->
  
    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(245, 244, 244, 0.9);">
      © 2021 Copyright:
      <a class="text-reset fw-bold" href="{{ route('home') }}">Presto.it</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->