    <!-- Navbar -->

    <nav class="navbar navbar-expand navbar-light d-flex justify-content-between">

        <div class="px-3">
            
            <a class="transition" href="{{ route('home') }}"><i class="fab fa-phoenix-squadron fs-2 yellow-icon"></i></a>
        
        </div>

        

        <div>

            <ul class="navbar-nav">

                <li class="nav-item pl-md-3">

                    <a class="nav-link transition" href="{{ route('announcement.create') }}"><i
                            class="fas fa-plus px-2"></i><span class="hide">{{__('pippo.create_announcement')}}</span></a>

                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle transition" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        <span class="hide">{{__('pippo.categories')}}</span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach ($categories as $category)
                            <li><a class="dropdown-item"
                                    href="{{ route('categories.index', ['name' => $category->name, 'id' => $category->id]) }}">{{ $category->name }}</a>
                            </li>
                        @endforeach
                        

                    </ul>

                </li>  
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle transition language" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-globe"></i></a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                       
                      <li class="d-inline-block">
                        <form action="{{route('local', 'it')}}" method="POST">
                            @csrf
                            <button type="submit" class="languages-box">
                                <span class="flag-icon flag-icon-it"></span>
                            </button>
                        </form>
                      </li>
                            
                      <li class="d-inline-block">
                        <form action="{{route('local', 'gb')}}" method="POST">
                            @csrf
                            <button type="submit" class="languages-box">
                                <span class="flag-icon flag-icon-gb"></span>
                            </button>
                        </form>
                      </li>
                                
                      <li class="d-inline-block">
                        <form action="{{route('local', 'fr')}}" method="POST">
                            @csrf
                          <button type="submit" class="languages-box">
                              <span class="flag-icon flag-icon-fr"></span>
                          </button>
                        </form>
                      </li>
                    </ul>
                  </li>

            </ul>


        </div>


        <div class="px-3">

            <ul class="navbar-nav">    
                <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle transition" href="#" id="navbarDropdown" role="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="far fa-user"></i></a>
                          
                    <ul class="dropdown-menu" style="left:auto; right:0;" aria-labelledby="navbarDropdown">
                    
                        @guest
            
                            
                                <li class="nav-item">
                                    <a class="nav-link transition" href="{{ route('register') }}" tabindex="-1">{{__('pippo.register')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link transition" href="{{ route('login') }}">Login</a>
                                </li>
                                
                                @else
                                @if (Auth::user()->is_revisor)
                                    <li class="nav-item">
                                        <a class="nav-link transition" href="{{ route('revisor.index') }}">{{__('pippo.revisor')}}
                                             
                                            <span class="badge badge-pill badge-warning" style="background-color: rgb(255, 196, 0);">{{\App\Models\Announcement::ToBeRevisionedCount()}}</span>
                                            
                                        </a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link active transition" aria-current="page" href="#">Ciao
                                        {{ Auth::user()->name }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link transition" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();document.getElementById('form-logout').submit();">Logout</a>
                                </li>
                                <form action="{{ route('logout') }}" method="POST" id="form-logout">
                                    @csrf
                                </form>

                        @endguest
                        
                    </ul>

                </li>   
                

            </ul>

        </div>        

    </nav>

    <div class="row pt-3 mx-0">

        <div class="col-12 d-flex justify-content-center px-0">
      
            <form class="form-inline my-2 my-lg-0 transition" method="get" action="{{route('search')}}">
            @csrf
                <input class="form-control mr-sm-2 search-bar d-inline-block" type="search" name="q" placeholder="{{__('pippo.search')}}" aria-label="Search">

                <button class="btn m-0 p-0" type="submit">
                
                    <i class="fas fa-search yellow-icon px-1"></i>
            
                </button>

            </form>
      
        </div>
      
    </div>

    
    
