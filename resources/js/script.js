// Index

const hero = document.querySelector('.hero');
const transition = document.querySelectorAll('.transition');
const alert = document.querySelectorAll('.alert');

const tl = new TimelineMax();

//Index

tl.fromTo(hero, 0.8, {height: "0%"}, {height: "80%", ease: Power2.easeInOut})
.fromTo(hero, 1, {width: "100%"}, {width: "80%", ease: Power2.easeInOut})
.fromTo(transition, 0.4, {opacity: 0, x: 30}, {opacity: 1, x: 0}, "-=0.4")
.fromTo(alert, 0.4, {opacity: 0, x: 30}, {opacity: 1, x: 0}, "-=0.4")





