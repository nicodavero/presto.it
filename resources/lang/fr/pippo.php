<?php

return [
    'welcome'=>'Bienvenue sur Presto.it',
    'create_announcement'=>'Insérer une annonce',
    'categories' => 'Catégories',
    'register' => 'Inscris-toi',
    'lastAds'=> 'Dernières annonces',
    'search' => 'Rechercher',
    'revisor'=> 'Réviseur',
    'sell'=>'Vends',
    'steps'=>', en trois steps',
    'deal' => 'Conclus la vente',
    'box' => 'Prépare le paquet en respectant les règles envoyés par mail',
    'delivery' => "Porte-le dans un de nos point de retrait. On s'occupe du reste!",
    'contacts'=>"Contacts",
    'details'=> 'En savoir plus',
    // create announcement view
    'create_title'=>'Titre',
    'create_description'=>'Description',
    'create_price'=>'Prix',
    'create_category'=>'Sélectionner la catégorie ',
    'create_images'=>'Insérer une image',
    'create_submit'=>'Envoyer'
];
