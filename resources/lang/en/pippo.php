<?php

return [
    'welcome'=>'Welcome to Presto.it',
    'create_announcement'=>'Add Announcement',
    'categories' => 'Categories',
    'register' => 'Sign up',
    'lastAds'=> 'Last Ads',
    'search' => 'Find',
    'revisor'=> 'Revisor',
    'sell'=>'Sell',
    'steps'=>', in three steps',
    'deal' => 'Conclude the deal',
    'box' => 'Prepare your parcel following our instructions sent by mail',
    'delivery' => "Bring it to one of our call points. We'll do the rest!",
    'contacts'=>"Contacts",
    'details'=> 'More info',
    // create announcement view
    'create_title'=>'Title',
    'create_description'=>'Description',
    'create_price'=>'Price',
    'create_category'=>'Select Category',
    'create_images'=>'Pictures',
    'create_submit'=>'Submit'
];

