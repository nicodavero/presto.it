<?php

return [
    'welcome'=>'Benvenuto su Presto.it',
    'create_announcement'=>'Inserisci annuncio',
    'categories' => 'Categorie',
    'register' => 'Registrati',
    'lastAds'=> 'Ultimi annunci',
    'search' => 'Cerca',
    'revisor'=> 'Revisore',
    'sell'=>'Vendi',
    'steps'=>', in tre passaggi',
    'deal' => 'Concludi la vendita',
    'box' => 'Prepara il pacco rispettando le istruzioni inviate tramite mail',
    'delivery' => "Portalo in uno dei nostri punti raccolta. Al resto ci pensiamo noi!",
    'contacts'=>"Contatti",
    'details'=> 'Scopri di più',
    // create announcement view
    'create_title'=>'Titolo',
    'create_description'=>'Descrizione',
    'create_price'=>'Prezzo',
    'create_category'=>'Seleziona categoria',
    'create_images'=>'Inserisci foto',
    'create_submit'=>'Invia'

];
