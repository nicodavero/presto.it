<?php

use App\Models\Announcement;
use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        $categories = ['Mobili', 'Auto', 'Motori', 'Informatica', 'Libri', 'Immobili', 'Smartphone', 'Giochi', 'Elettrodomestici', 'Lavoro'];

        foreach ($categories as $category) {
            $category_new = new Category();
            $category_new->name = $category;
            $category_new->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
